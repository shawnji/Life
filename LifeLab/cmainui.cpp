#include "cmainui.h"
#include "ui_cmainui.h"

CMainUI::CMainUI(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CMainUI)
{
    ui->setupUi(this);
}

CMainUI::~CMainUI()
{
    delete ui;
}
