#ifndef CMAINUI_H
#define CMAINUI_H

#include <QWidget>

namespace Ui {
class CMainUI;
}

class CMainUI : public QWidget
{
    Q_OBJECT

public:
    explicit CMainUI(QWidget *parent = 0);
    ~CMainUI();

private:
    Ui::CMainUI *ui;
};

#endif // CMAINUI_H
