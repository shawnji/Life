#include "cmainui.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CMainUI w;
    w.show();

    return a.exec();
}
